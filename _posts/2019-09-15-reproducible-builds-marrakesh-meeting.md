---
layout: post
title:  "Reproducible Builds Marrakesh meeting"
date:   2019-09-15 13:37:42
categories: org
draft: false
---

People interested in reproducible builds will meet again! This will be the fifth summit following our [December 2018 summit in Paris]({{ "/events/paris2018/" | relative_url }}), the [November 2017 summit in Berlin]({{ "/events/berlin2017/" | relative_url }}), the [December 2016 summit in Berlin]({{ "/events/berlin2016/" | relative_url }}) and our [December 2015 summit in Athens]({{ "/events/athens2015/" | relative_url }}).

Compared to previous summits the format will slightly change: this time, the three core days will be surrounded by up to five days for hacking and further collaboration.

Like last years at this now (up to) [eight-day event in Marakesh]({{ "/events/Marrakesh2019/" | relative_url }}) we will welcome both previous attendees and new projects alike. The participants will discuss, connect and exchange ideas in order to grow the reproducible builds effort. We will bring this space into life:

 <img src="/images/marrakesh2019/IMG_20190311_090022.jpg" width="640px" />

Whilst the exact content of the meeting will be shaped by the participants, the main goals will include:

  * Update & exchange about the status of reproducible builds in various projects.
  * Improve collaboration both between and inside projects.
  * Expand the scope and reach of reproducible builds to more projects.
  * Work together and hack on solutions.
  * Establish space for more strategic and long-term thinking than is possible in virtual channels.
  * Brainstorm designs on tools enabling users to get the most benefits from reproducible builds.
  * Discuss how reproducible builds will be usable and meaningful to users and developers alike.

Logs and minutes will be published after the meeting.

Please [reach out]({{ "/events/Marrakesh2019" | relative_url }}) if you'd like to participate in hopefully interesting, inspiring and intense technical sessions about reproducible builds and beyond!

