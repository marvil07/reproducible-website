---
layout: report
year: "2021"
month: "11"
title: "Reproducible Builds in November 2021"
draft: false
date: 2021-12-05 18:33:21
---

[![]({{ "/images/reports/2021-11/reproducible-builds.png#right" | relative_url }})](https://reproducible-builds.org/)

Welcome to the November 2021 report from the [Reproducible Builds](https://reproducible-builds.org) project.

As a quick recap, whilst anyone may inspect the source code of free software for malicious flaws, almost all software is distributed to end users as pre-compiled binaries. The motivation behind the reproducible builds effort is therefore to ensure no flaws have been introduced during this compilation process by promising identical results are always generated from a given source, thus allowing multiple third-parties to come to a consensus on whether a build was compromised. If you are interested in contributing to our project, please visit our [*Contribute*]({{ "/contribute/" | relative_url }}) page on our website.

<br>

[![]({{ "/images/reports/2021-11/seagl.png#right" | relative_url }})](https://seagl.org/archive/2021/debugging-reproducible-builds-one-day-at-a-time)

On November 6th, Vagrant Cascadian presented at this year's edition of the [SeaGL](https://seagl.org/) conference, giving a talk titled [*Debugging Reproducible Builds One Day at a Time*](https://seagl.org/archive/2021/debugging-reproducible-builds-one-day-at-a-time):

> I'll explore how I go about identifying issues to work on, learn more about the specific issues, recreate the problem locally, isolate the potential causes, dissect the problem into identifiable parts, and adapt the packaging and/or source code to fix the issues.

A [video recording of the talk](https://archive.org/embed/SeaGL2021-Vagrant_Cascadian-Debugging_Reproducible_Builds_One_Day_at_a_Time) is available on *archive.org*.

<br>

[![]({{ "/images/reports/2021-11/fedoramagazine.png#right" | relative_url }})](https://seagl.org/archive/2021/debugging-reproducible-builds-one-day-at-a-time)

[Fedora Magazine](https://fedoramagazine.org/) published a post written by [Zbigniew Jędrzejewski-Szmek](https://fedoraproject.org/wiki/User:Zbyszek) about how to [*Use Diffoscope in packager workflows*](https://fedoramagazine.org/use-diffoscope-in-packager-workflows/), specifically around ensuring that new versions of a package do not introduce breaking changes:

> In the role of a packager, updating packages is a recurring task. For some projects, a packager is involved in upstream maintenance, or well written release notes make it easy to figure out what changed between the releases. This isn't always the case, for instance with some small project maintained by one or two people somewhere on GitHub, and it can be useful to verify what exactly changed. [Diffoscope](https://diffoscope.org/) can help determine the changes between package releases. [[...](https://fedoramagazine.org/use-diffoscope-in-packager-workflows/)]

<br>

*kpcyrd* [announced](https://lists.reproducible-builds.org/pipermail/rb-general/2021-November/002437.html) the release of [`rebuilderd`](https://rebuilderd.com/) version 0.16.3 on [our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/) this month, adding support for builds to generate multiple artifacts at once.

<br>

[![]({{ "/images/reports/2021-11/ircmeeting.png#right" | relative_url }})](http://meetbot.debian.net/reproducible-builds/2021/reproducible-builds.2021-11-30-15.02.html)

Lastly, we [held another IRC meeting on November 30th](https://lists.reproducible-builds.org/pipermail/rb-general/2021-November/002438.html). As mentioned in previous reports, due to the global events throughout 2020 etc. there will be [no in-person summit event this year](https://lists.reproducible-builds.org/pipermail/rb-general/2020-September/002045.html).

<br>

## [*diffoscope*](https://diffoscope.org)

[![]({{ "/images/reports/2021-11/diffoscope.svg#right" | relative_url }})](https://diffoscope.org)

[*diffoscope*](https://diffoscope.org) is our in-depth and content-aware diff utility. Not only can it locate and diagnose reproducibility issues, it can provide human-readable diffs from many kinds of binary formats. This month, Chris Lamb made the following changes, including preparing and uploading versions `190`, `191`, `192`, `193` and `194` to Debian:

* New features:

    * Continue loading a `.changes` file even if the referenced files do not exist, but include a comment in the returned diff. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/e9839553)]
    * Log the reason if we cannot load a Debian `.changes` file. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/63ffe02e)]

* Bug fixes:

    * Detect XML files as XML files if `file(1)` claims if they are XML files or if they are named `.xml`. ([`#999438`](https://bugs.debian.org/999438))
    * Don't duplicate file lists at each directory level. ([`#989192`](https://bugs.debian.org/989192))
    * Don't raise a traceback when comparing nested directories with non-directories. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/78aae03c)]
    * Re-enable `test_android_manifest`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/21cc83e7)]
    * Don't reject Debian `.changes` files if they contain non-printable characters. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/c45f587b)]

* Codebase improvements:

    * Avoid aliasing variables if we aren't going to use them. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/f13c6859)]
    * Use `isinstance` over `type`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/491be46e)]
    * Drop a number of unused imports. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/28d0e77d)]
    * Update a bunch of `%`-style string interpolations into f-strings or `str.format`. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/9bf746ab)]
    * When pretty-printing JSON, mark the difference as being reformatted, additionally avoiding including the full path. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/0c907dc8)]
    * Import `itertools` top-level module directly. [[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/592c401b)]

Chris Lamb also made an update to the command-line client to *[trydiffoscope](https://try.diffoscope.org)*, a web-based version of the [diffoscope](https://diffoscope.org) in-depth and content-aware diff utility, specifically only waiting for 2 minutes for `try.diffoscope.org` to respond in tests. ([#998360](https://bugs.debian.org/998360))

In addition Brandon Maier corrected an issue where parts of large diffs were missing from the output&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/6790469f)], Zbigniew Jędrzejewski-Szmek fixed some logic in the `assert_diff_startswith` method&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/aea11a3b)] and Mattia Rizzolo updated the packaging metadata to denote that we support both Python 3.9 and 3.10&nbsp;[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/3f09bbc7)] as well as a number of warning-related changes[[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/3b5f695b)][[...](https://salsa.debian.org/reproducible-builds/diffoscope/commit/ae946862)]. Vagrant Cascadian also updated the *diffoscope* package in [GNU Guix](https://www.gnu.org/software/guix/)&nbsp;[[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=3827c66dc1d9760f6f7f4753a518124075f10e64)][[...](https://git.savannah.gnu.org/cgit/guix.git/commit/?id=dc05fea1db2e868212048c3d94c0e30e2768f2c1)].

<br>

## Distribution work

[![]({{ "/images/reports/2021-11/debian.png#right" | relative_url }})](https://debian.org/)

In Debian, Roland Clobus updated the [wiki page documenting Debian reproducible 'Live' images](https://wiki.debian.org/ReproducibleInstalls/LiveImages) to mention some new bug reports and also [posted an in-depth status update](https://lists.reproducible-builds.org/pipermail/rb-general/2021-November/002436.html) to [our mailing list](https://lists.reproducible-builds.org/pipermail/rb-general/).

In addition, 90 reviews of Debian packages were added, 18 were updated and 23 were removed this month adding to [our knowledge about identified issues](https://tests.reproducible-builds.org/debian/index_issues.html). Chris Lamb identified a new toolchain issue, [`absolute_path_in_cmake_file_generated_by_meson](https://salsa.debian.org/reproducible-builds/reproducible-notes/commit/5c3aeb2d).

<br>

[![]({{ "/images/reports/2021-11/archlinux.png#right" | relative_url }})](https://archlinux.org/)

Work has begun on classifying reproducibility issues in packages within the [Arch Linux](https://archlinux.org/) distribution. Similar to the analogous effort within Debian (outlined above), package information is listed in a human-readable [`packages.yml` YAML file](https://gitlab.archlinux.org/archlinux/reproducible-archlinux-notes/-/blob/main/packages.yml) and a sibling [`README.md`](https://gitlab.archlinux.org/archlinux/reproducible-archlinux-notes/-/blob/main/README.md) file shows how to classify packages too.

[![]({{ "/images/reports/2021-12/opensuse.png#right" | relative_url }})](https://www.opensuse.org/)

Finally, Bernhard M. Wiedemann posted his [monthly reproducible builds status report](https://lists.opensuse.org/archives/list/factory@lists.opensuse.org/thread/S5CIAMYJAEHKTPUDWI5U6YGZDTUP46TT/) for [openSUSE](https://www.opensuse.org/) and Vagrant Cascadian updated a link [on our website](https://reproducible-builds.org) to link to the [GNU Guix reproducibility testing overview](https://data.guix.gnu.org/repository/1/branch/master/latest-processed-revision/package-reproducibility)&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reproducible-website/commit/c8d19cb)].

<br>

## Software development

The Reproducible Builds project detects, dissects and attempts to fix as many currently-unreproducible packages as possible. We endeavour to send all of our patches upstream where appropriate. This month, we wrote a large number of such patches, including:

* Bernhard M. Wiedemann:

    * [`ck`](https://github.com/concurrencykit/ck/pull/184) (build failure in single-CPU machine)
    * [`libfabric`](https://github.com/ofiwg/libfabric/pull/7250) (date-related issue)
    * [`python-dukpy-kovidgoyal`](https://build.opensuse.org/request/show/933425) (filesystem ordering)
    * [`python-thriftpy2`](https://github.com/Thriftpy/thriftpy2/issues/182) (build failure in the far future)
    * [`smplayer`](https://github.com/smplayer-dev/smplayer/pull/265) (date)

* Chris Lamb:

    * [#998312](https://bugs.debian.org/998312) filed against [`ibus-input-pad`](https://tracker.debian.org/pkg/ibus-input-pad).
    * [#999848](https://bugs.debian.org/999848) filed against [`node-cssstyle`](https://tracker.debian.org/pkg/node-cssstyle).
    * [#999866](https://bugs.debian.org/999866) filed against [`liboqs`](https://tracker.debian.org/pkg/liboqs).
    * [#1000326](https://bugs.debian.org/1000326) filed against [`xrstools`](https://tracker.debian.org/pkg/xrstools).
    * [#1000327](https://bugs.debian.org/1000327) filed against [`meson`](https://tracker.debian.org/pkg/meson) ([forwarded](https://github.com/mesonbuild/meson/pull/9602)).
    * [#1000401](https://bugs.debian.org/1000401) filed against [`golang-github-go-git-go-git`](https://tracker.debian.org/pkg/golang-github-go-git-go-git).
    * [#1000531](https://bugs.debian.org/1000531) filed against [`sphinxcontrib-applehelp`](https://tracker.debian.org/pkg/sphinxcontrib-applehelp).
    * [#1000532](https://bugs.debian.org/1000532) filed against [`sphinxcontrib-jsmath`](https://tracker.debian.org/pkg/sphinxcontrib-jsmath).
    * [#1000533](https://bugs.debian.org/1000533) filed against [`sphinxcontrib-htmlhelp`](https://tracker.debian.org/pkg/sphinxcontrib-htmlhelp).
    * [#1000535](https://bugs.debian.org/1000535) filed against [`sphinxcontrib-restbuilder`](https://tracker.debian.org/pkg/sphinxcontrib-restbuilder).
    * [#1000769](https://bugs.debian.org/1000769) filed against [`node-marked`](https://tracker.debian.org/pkg/node-marked).
    * [#1000770](https://bugs.debian.org/1000770) filed against [`perfect-scrollbar`](https://tracker.debian.org/pkg/perfect-scrollbar).

* Roland Clobus:

    * [#1000674](https://bugs.debian.org/1000674) and [#1000685](https://bugs.debian.org/1000685) filed against [`dictionaries-common`](https://tracker.debian.org/pkg/dictionaries-common) (randomness in Ispell dictionaries)

* Simon McVittie:

    * [#999552](https://bugs.debian.org/999552) filed against [`pcs`](https://tracker.debian.org/pkg/pcs).

* Vagrant Cascadian:

    * [#998420](https://bugs.debian.org/998420) filed against [`minia`](https://tracker.debian.org/pkg/minia).
    * [#1000768](https://bugs.debian.org/1000768) filed against [`krb5`](https://tracker.debian.org/pkg/krb5).
    * [#1000836](https://bugs.debian.org/1000836) filed against [`libu2f-host`](https://tracker.debian.org/pkg/libu2f-host).
    * [#1000839](https://bugs.debian.org/1000839) filed against [`gutenprint`](https://tracker.debian.org/pkg/gutenprint).
    * [#1000893](https://bugs.debian.org/1000893) filed against [`bind9`](https://tracker.debian.org/pkg/bind9).
    * [#1000897](https://bugs.debian.org/1000897) filed against [`lift`](https://tracker.debian.org/pkg/lift).
    * [#1000921](https://bugs.debian.org/1000921) filed against [`syncevolution`](https://tracker.debian.org/pkg/syncevolution).
    * [#1000944](https://bugs.debian.org/1000944) filed against [`apbs`](https://tracker.debian.org/pkg/apbs).
    * [#1000945](https://bugs.debian.org/1000945) filed against [`binutils-riscv64-unknown-elf`](https://tracker.debian.org/pkg/binutils-riscv64-unknown-elf).
    * [#1000946](https://bugs.debian.org/1000946) filed against [`gcc-riscv64-unknown-elf`](https://tracker.debian.org/pkg/gcc-riscv64-unknown-elf).

    * [`opensbi`](https://github.com/riscv-software-src/opensbi/pull/229), which [later led to a better patch](https://lists.infradead.org/pipermail/opensbi/2021-November/002199.html) on their mailing list.

Elsewhere, in software development, Jonas Witschel updated [*strip-nondeterminism*](https://tracker.debian.org/pkg/strip-nondeterminism), our tool to remove specific non-deterministic results from a completed build so that it did not fail on JAR archives containing invalid members with a .jar extension&nbsp;[[...](https://salsa.debian.org/reproducible-builds/strip-nondeterminism/commit/763f4f8)]. This change was later uploaded to Debian by Chris Lamb.

[*reprotest*](https://tracker.debian.org/pkg/reprotest) is the Reproducible Build's project end-user tool to build the same source code twice in widely different environments and checking whether the binaries produced by the builds have any differences. This month, Mattia Rizzolo overhauled the Debian packaging&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/56628ec)][[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/2b80d38)][[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/25f83c8)] and fixed a bug surrounding suffixes in the Debian package version&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/ffe2df8)], whilst Stefano Rivera fixed an issue where the package tests were broken [after the removal of *diffoscope* from the package's strict dependencies](https://bugs.debian.org/988964)&nbsp;[[...](https://salsa.debian.org/reproducible-builds/reprotest/commit/3ed09e3)].

<br>

## Testing framework

[![]({{ "/images/reports/2021-11/testframework.png#right" | relative_url }})](https://tests.reproducible-builds.org/)

The Reproducible Builds project runs a testing framework at [tests.reproducible-builds.org](https://tests.reproducible-builds.org), to check packages and other artifacts for reproducibility. This month, the following changes were made:

* Holger Levsen:

    * Document the progress in setting up `snapshot.reproducible-builds.org`.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/4ca285db)]
    * Add the packages required for [*debian-snapshot*](https://github.com/fepitre/debian-snapshot).&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/6a75e4a8)]
    * Make the `dstat` package available on all Debian based systems.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/7fdab20a)]
    * Mark `virt32b-armhf` and `virt64b-armhf` as down.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/73c96987)]

* Jochen Sprickerhof:

    * Add SSH authentication key and enable access to the `osuosl168-amd64` node.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/09f9dd8f)][[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/a165f96a)]

* Mattia Rizzolo:

    * Revert "reproducible Debian: mark virt(32|64)b-armhf as down" - restored.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/983b5ba8)]

* Roland Clobus (Debian "live" image generation):

    * Rename `sid` internally to `unstable` until [an issue in the snapshot system](https://github.com/fepitre/debian-snapshot/issues/7) is resolved.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/883c311b)]
    * Extend testing to include Debian *bookworm* too..&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/d455e466)]
    * Automatically create the Jenkins 'view' to display jobs related to building the Live images.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/f5927cd9)]

* Vagrant Cascadian:

    * Add a Debian 'package set' group for the packages and tools maintained by the Reproducible Builds maintainers themselves.&nbsp;[[...](https://salsa.debian.org/qa/jenkins.debian.net/commit/20bf578c)]

<br>

---

If you are interested in contributing to the Reproducible Builds project, please visit our [*Contribute*](https://reproducible-builds.org/contribute/) page on our website. However, you can get in touch with us via:

 * IRC: `#reproducible-builds` on `irc.oftc.net`.

 * Twitter: [@ReproBuilds](https://twitter.com/ReproBuilds)

 * Mailing list: [`rb-general@lists.reproducible-builds.org`](https://lists.reproducible-builds.org/listinfo/rb-general)
